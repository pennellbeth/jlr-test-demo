# export PYTHONPATH=src/

python := python3.8

up:
	docker-compose up -d --build

down:
	docker-compose down

test-pact-consumer:
	rm -f front-end/tests/contract/pacts/*
	cd front-end/ && \
	    npm run test:consumer

test-pact-provider:
	cd back-end/ && \
		. venv/bin/activate && \
		docker-compose up --build back-end -d && \
	    pact-verifier --provider-base-url=http://localhost:5000 --pact-url=../front-end/tests/contract/pacts --provider-states-setup-url=http://localhost:5000/provider-states

test-pact:
	rm -f front-end/tests/contract/pacts/*
	cd front-end/ && \
	    npm run test:consumer && \
	cd back-end/ && \
		. venv/bin/activate && \
	    docker-compose up --build back-end -d && \
	    pact-verifier --provider-base-url=http://localhost:5000 --pact-url=../front-end/tests/contract/pacts --provider-states-setup-url=http://localhost:5000/provider-states

test-postman:
	docker-compose up --build back-end -d && \
	newman run back-end/tests/postman/api-demo.postman_collection.json -e back-end/tests/postman/local.postman_environment.json