# JLR Test Demo

![JLR Logo](docs/images/JLR-Logo.png)

## Description

Using a simple audit log application, this repo demonstrates different API testing
techniques including integration testing, functional testing, contract testing and API
performance testing. All but the latter then run as stages in a GitLab CI/CD pipeline.

It is supported by documentation files outlining and breaking down the test approach
used, risks identified, coverage and tooling choices made.

It is not intended to be an exhaustive list of all tests for this sample audit API, but
merely a showcase of testing types to consider when dealing with APIs, and how to
integrate these into your workflow and CI/CD pipeline.


## Getting Started

The whole stack can be run in docker using different docker-compose profiles
(Note: Requires at least Docker-Compose 1.28)

- `docker-compose up -d` to bring up the application containers
- `docker-compose up --build back-end` to bring up the back-end API and DB it relies
  on for testing just the API
- `docker-compose --profile testing up` to bring up all of the containers including
  the Pact Broker and Newman for running the Postman collection locally.

**Note:** If you see errors in the `back-end` logs of the nature of 
`relation audit_log does not exist`, try running `docker-compose restart back-end` to 
restart that service and re-run the migrations.

The front-end should be available at http://localhost:8081

The API should be available at http://localhost:5000 and the API docs at 
http://localhost:5000/docs

There is a helper script to populate the application database with some log entries 
with the following command:
`docker-compose exec back-end scripts/make_log_entries.py`

### Contributing

The front-end service is written in NodeJS, and dependency management is handled through npm.
The back-end service is written in Python, and dependency management is handled through
a requirements.txt file.

**Installing Dependencies**
- Front-end service - from the front-end service folder, run `npm i`
- Back-end service - from the back-end service folder, ensure you're using a
  python venv with `. venv/bin/activate`, then run `pip install -r requirements.txt`

The Makefile also has some commands which can be run from the root directory - refer to
contract testing block below for more details.

## Testing

The test approach and rationale used for this application is documented in the /docs 
folder.

### Integration Testing
- Integration tests for the provider are run with `pytest`

### Functional Testing

Functional tests for the provider have been conducted through Postman. The `back-end`
service will need to be running for this, which can be brought up by running
`docker-compose up --build back-end`

- Manual Testing - Import the collection file found in back-end/tests/postman
  into your own Postman client or browser.
- Newman - The collection tests can either be run against Newman on your local machine
  with `make test-postman` or against the Newman docker container with 
  `docker-compose up newman`

### Contract Testing

Contract testing has been implemented using PACT-JS and PACT-Python for the front-end
and back-end services respectively.

There are makefile commands to simplify the running of the tests across the two:
- `make test-pact-consumer` runs the consumer side test, and creates a pact file
  in front-end/tests/contract/pacts
- `make test-pact-provider` runs the provider side verification. This can be
  against a pact file or a published pact in the Pact Broker by changing the
  `--pact-url`
    - To run against a local pact file, change the `--pact-url` to the relative file
      path including the filename
      (e.g. `--pact-url=../front-end/tests/contract/pacts/frontend-backend.json`)
    - To run against a published pact in the broker, the `--pact-url` needs to point
      to an endpoint with the following pattern:
      `http://localhost:9292/pacts/provider/<PROVIDER-NAME>/consumer/<CONSUMER-NAME>/version/<CONSUMER-APP-VERSION-#>`
    - **NB.** Ensure that the provider version is accurate when verifying. Change this
      by altering the `-a` flag value
- `make test-pact` runs the consumer test and provider verification steps in one 
  (provider notes are relevant here too).

### Performance Testing

Ensure you're in the back-end folder venv with `. venv/bin/activate`.

`locust --help` for full list of arguments

Run headless with `locust -H wfng -u 10 -r 1 --headless -t 180s --only-summary`

## CI/CD Pipeline

This repo uses GitLab for its CI/CD and it has the following stages:
- lint
- test
    - integration-tests
    - postman-tests
    - pact-test
- publish
    - pact-publish
- verify
    - pact-verify
- deploy
    - can-i-deploy