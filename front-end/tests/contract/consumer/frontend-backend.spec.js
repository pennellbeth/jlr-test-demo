'use strict'

const { _fetchLogs } = require('../../../src/services/auditLog.js')
const path = require('path')
const Pact = require('@pact-foundation/pact').Pact
const { eachLike } = require('@pact-foundation/pact/src/dsl/matchers')

const PORT = 5000
const URL = 'http://localhost'

const mockProvider = new Pact({
  consumer: 'Frontend',
  provider: 'Backend',
  port: PORT,
  log: path.resolve(process.cwd(), 'tests/contract/logs', 'pact-mockserver.log'),
  dir: path.resolve(process.cwd(), 'tests/contract/pacts'),
  logLevel: 'INFO',
  spec: 2,
  pactfileWriteMode: 'overwrite'
})

describe('Audit Log Service', () => {
  const GET_EXPECTED_BODY = {
    id: '2b4881cc-b3be-4a56-8fa9-a7bb5bc46b2c',
    timestamp: '2021-02-01T00:00:00',
    event_source: 'Not loaded yet',
    event_type: 'Something',
    message: 'This is a message'
  }

  describe('GET audit log entries', () => {
    beforeAll(() =>
      // Set up the mock provider
      mockProvider.setup().then(() => {
        const interaction = {
          state: 'I have existing log entries',
          uponReceiving: 'a request for all logs',
          withRequest: {
            method: 'GET',
            path: '/log',
            headers: {
              Accept: 'application/json, text/plain, */*'
            }
          },
          willRespondWith: {
            status: 200,
            headers: {
              'Content-Type': 'application/json'
            },
            // Every child in the array will match the example types
            body: eachLike(GET_EXPECTED_BODY)
          }
        }

        return mockProvider.addInteraction(interaction)
      })
    )

    test('Returns correct statusCode, header and body', async () => {
      const response = await _fetchLogs(URL, PORT)
      expect(response.status).toEqual(200)
      expect(response.headers['content-type']).toBe('application/json')
      expect(response.data).toEqual([GET_EXPECTED_BODY])
    })

    afterEach(() => mockProvider.verify())
    afterAll(() => mockProvider.finalize())
  })
})
