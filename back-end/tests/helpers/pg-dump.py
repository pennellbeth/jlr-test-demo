"""Capture the current rows of a PostgresQL database table and store as a JSON file"""

import psycopg2
import json
from datetime import date, datetime


def db_connection(args):
    conn = psycopg2.connect(
        dbname=args.dbname, user=args.user, password=args.password, host=args.host
    )
    return conn.cursor()


def row_to_dict(cursor, column_names: list):
    for row in cursor.fetchall():
        yield dict(zip(column_names, row))


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (date, datetime)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))


def json_writer(path, list_of_dicts):
    with open(path, "w") as file:
        file.write(json.dumps(list_of_dicts, indent=2, default=json_serial))


if __name__ == "__main__":
    import argparse

    # Create the parser
    dump_parser = argparse.ArgumentParser(description="Dump postgres table to json")

    # Add the arguments
    dump_parser.add_argument(
        "--path",
        type=str,
        required=True,
        help="Path including filename to write the json to",
    )

    dump_parser.add_argument(
        "-H", "--host", type=str, default="127.0.0.1", help="Hostname or IP of database"
    )

    dump_parser.add_argument(
        "-p", "--port", type=str, default="5432", help="Database port"
    )

    dump_parser.add_argument(
        "-U", "--user", type=str, default="postgres", help="Username for postgres"
    )

    dump_parser.add_argument(
        "-W", "--password", type=str, default="password", help="Password for db user"
    )

    dump_parser.add_argument(
        "--dbname", type=str, default="postgres", help="Name of database"
    )

    dump_parser.add_argument(
        "--table", type=str, required=True, help="Database table to dump"
    )

    # Execute the parse_args() method
    args = dump_parser.parse_args()

    cur = db_connection(args)
    cur.execute(f"SELECT * FROM {args.table};")
    colnames = [description[0] for description in cur.description]
    rows = list(row_to_dict(cursor=cur, column_names=colnames))
    json_writer(args.path, rows)
