import base64
import os
import pytest
from fastapi.testclient import TestClient
from sqlalchemy import create_engine, select
from sqlalchemy.orm import sessionmaker

from api.api import app, get_db
from api.models import AuditLog, Base


@pytest.fixture
def db_session():
    """Create and return a TestingSessionLocal()."""
    db_url = os.getenv("db_url")
    engine = create_engine(db_url)
    TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    Base.metadata.create_all(bind=engine)

    db = TestingSessionLocal(bind=engine)

    yield db

    db.rollback()
    Base.metadata.drop_all(bind=engine)
    db.close()


@pytest.fixture
def api_client(db_session):
    def override_get_db():
        yield db_session

    app.dependency_overrides[get_db] = override_get_db
    client = TestClient(app)
    return client


@pytest.fixture
def auth_header():
    auth_string = "user:password"
    encoded_auth = base64.b64encode(auth_string.encode()).decode()
    return f"Basic {encoded_auth}"


def test_read_log(db_session, api_client):
    """Confirm that items written to the database can be retrieved"""
    log_entry = AuditLog(event_source="test", event_type="INFO", message="foo")
    db_session.add(log_entry)
    db_session.commit()

    result = api_client.get("/log")

    expected_result = {
        "id": str(log_entry.id),
        "timestamp": log_entry.timestamp.isoformat(),
        "event_source": "test",
        "event_type": "INFO",
        "message": "foo",
    }

    assert result.status_code == 200
    assert result.json() == [expected_result]


def test_post_log(db_session, api_client, auth_header):
    """Confirm items are written to the test database"""
    api_client.post(
        "/log",
        headers={"Authorization": auth_header},
        json={
            "event_source": "Audit Log 3",
            "event_type": "INFO",
            "message": "Calling occupants of interplanetary craft",
        },
    )

    api_client.post(
        "/log",
        headers={"Authorization": auth_header},
        json={
            "event_source": "Audit Log 1",
            "event_type": "ERROR",
            "message": "Unrecognised Command: 'Bazinga'",
        },
    )

    items = list(db_session.execute(select(AuditLog)))
    assert len(items) == 2
