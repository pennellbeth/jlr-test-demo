from locust import TaskSet, task, tag
from time import sleep


class APITasks(TaskSet):
    """The task set that the locust users will select from"""

    @tag("get", "all")
    @task(1)
    def basic_get(self):
        self.client.get("/log")
        sleep(1)

    @tag("post", "all")
    @task(4)
    def basic_post(self):
        self.client.post(
            "/log",
            {
                "event_source": "Test Log",
                "event_type": "Test Type",
                "message": "Test Message",
            },
        )
        sleep(1)
