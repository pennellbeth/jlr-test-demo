from locust import HttpUser, constant
from .tasks import APITasks


class APIUser(HttpUser):
    # can use between(x, y) for more randomised output
    wait_time = constant(1)

    def on_start(self):
        """on_start is called when a Locust user starts before any task is scheduled"""
        # Not required for this application, as Basic Auth can be handled on the CLI
        # commands for Locust. But useful for where all users must hit a login API first
        pass

    def on_stop(self):
        """on_stop is called when the TaskSet stops"""
        # Again, not necessary for this application, but useful for cleanup per user.
        pass

    task_set = APITasks
