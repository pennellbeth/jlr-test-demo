from dotenv import load_dotenv
from pydantic import BaseSettings


class Settings(BaseSettings):
    """Settings for the API."""

    debug: bool = False
    db_url: str
    host: str = "0.0.0.0"
    port: int = 5000
    api_user: str
    api_pass: str


load_dotenv()
settings = Settings()
