import enum


class EventType(enum.Enum):
    """The types of events which our audit log can handle."""

    ERROR = "error"
    CUSTOM = "custom"
