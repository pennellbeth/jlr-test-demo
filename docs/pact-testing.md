# Introduction
For the Provider (written in Python):
- Pact-Python - https://github.com/pact-foundation/pact-python

For the Consumer (written in JS):
- Pact-JS - https://github.com/pact-foundation/pact-js

Differences in implementation but both repos follow the basic outline of a contract test
with PACT:
- Consumer-side test which creates the interaction (contract)
- (Optional) Pact file published to the Pact Broker
- Provider-side verification of the contract
- (CI/CD) Version compatibility checked with can-i-deploy tool

**Note:** This repo utilises Pactflow, which is a managed version of the Pact Broker.
The Broker can be managed without Pactflow, but frankly it's a pain no-one needs to 
deal with when Pactflow is free and we're trying to balance CI/CD workflows.

## Consumer (front-end UI)

Pact is a consumer-driven contract testing tool, and as such it is the consumer that
drives the narrative on its expectations of the provider service.

However it is important to ensure that the contract tests do not become too perscriptive
so as to stop fluid development for both consumers and providers. The general rule for
contract testing with Pact is to keep the contract scope strictly to the functionality/
assertions that would break the consumer.

For example, this application returns an `id` field which is a UUID. Within the contract
it could be asserted that the ID match a type of a UUID, or a regex of the same. But
what for? The UI is just displaying an informational field, and treats it as a string
regardless. So the contract assertion can be a string, and we can add functional testing
around known business logic like this in functional tests for the Provider.

**Golden Rule:** If it won't break the consumer, don't include it in the contract.
Test it elsewhere.

### Local Development

For local development the flow is as follows:

![Consumer Local Flow](images/ConsumerLocalFlow.PNG)

1) Write a consumer test file which outlines the all of the `interactions` between a
   particular consumer and provider pair
2) Running the test with `npm run test:consumer` in the service folder (or 
   `make test-pact-consumer` in the root) will create the contract file in 
   `/tests/contract/pacts`
3) The contract file can then be published to the central Pact Broker with 
   `npm run pact:publish` or `make test-pact-publish` using the config in `pact-publish.js`

When using Pactflow, Pact keeps a permanent Broker up and handles all of the version 
management of the contracts automatically, along with compatability history. Therefore 
if development on the consumer (which increments the consumer version number) doesn't
materially affect the contract, then the original contract version will remain.

More details on versioning here: [https://docs.pact.io/getting_started/versioning_in_the_pact_broker/]

If development on the consumer does impact the contract, then the contract version will
be incremented.

Compatibility of various consumer and provider version is also managed by Pactflow as it
retains a matrix of current and previously verified contracts. 

Within this repo, local verification from a file can be performed by following the 
Provider local development section below.

### CI/CD Pipeline

![Consumer CI/CD Diagram](images/ConsumerPactFlow.png)

Using Pactflow, published pacts for this repo can be viewed at: [https://pennellbeth.pactflow.io/]

The above workflow is the one endorsed by Pactflow. As this repo does not have multiple
deployment environments to follow development through into test and prod environments,
not all of the above is possible. But this repo does show a simplified version to
showcase pact creation, centralised publishing and pact management, verification and
utilisation of the matrix.

In this repo, our CI/CD jobs are:
 - lint
 - integration-tests
 - postman-tests
 - pact-test
 - pact-publish
 - pact-verify
 - can-i-deploy

## Provider (back-end API)

In a consumer-driven contract world where contracts are written only for the minimum
requirements of the consumer, the provider is less restricted on continued development.
Additional fields or information passed from an endpoint will be ignored, provided that
the minimum contract requirements are still met.

From the provider side, the main requirement is verification of consumer contracts.

### Local Development

For local development the flow is as follows:

![Provider Local Flow](images/ProviderLocalFlow.PNG)

Pact-Python has an in-built verifier tool which can be run using CLI commands. You can
run `pact-verifier --help` for a full list of options.

The verifier can either be run against a local contract file, or against a contract
stored in the Pact Broker as follows:
- Local pact file - Use the `--pact-url` to the relative file path including the filename
  (e.g. `--pact-url=../front-end/tests/contract/pacts/frontend-backend.json`)
- Pact broker - Use the `--pact-broker-url` and point to `http://<BROKER-URL>:9292/` 

**NB.** Ensure that the provider has a compatible provider state file to get the DB set
up in the way you need to verify the contract. This is done by passing `--provider-state-url`
with the `http://<PROVIDER-URL>:<PORT>/provider-states` URL.

You can create a new provider state using the `back-end/tests/helpers/pg-dump.py` script.

When using the Pact Broker, Pact handles all of the version management and compatibility
automatically. If development on the provider (which increments the provider version) can
still verify existing contracts, then this verification is stored against the new provider
version in the Pact Matrix.
The Matrix is used during the `can-i-deploy` step of a CI/CD release.

If development on the provider does impact the contract, then the verification will fail.
With Pactflow, this failure will be stored within the Matrix.

### CI/CD Pipeline

![Consumer CI/CD Diagram](images/ProviderPactFlow.png)

Using Pactflow, current pacts for all consumers can be viewed at [https://pennellbeth.pactflow.io/]

Webhooks can be added as an option for alerting when contracts have failed / been changed.

The above workflow is the one endorsed by Pactflow. As this repo does not have multiple
deployment environments to follow development through into test and prod environments,
not all of the above is possible. But this repo does show a simplified version to
showcase pact creation, centralised publishing and pact management, verification and
utilisation of the matrix.

In this repo, our CI/CD jobs are:
 - lint
 - integration-tests
 - postman-tests
 - pact-test
 - pact-publish
 - pact-verify
 - can-i-deploy